from flask import Flask
from myapi import api
from myapi import manage
from myapi.config import get_config
from myapi.extensions import apispec
import myapi.adapters.orm as orm
from sqlalchemy import create_engine, inspect
from sqlalchemy.orm import sessionmaker
# from myapi.extensions import migrate


def create_app(config="production"):
    """Application factory, used to create application"""
    app = Flask("myapi")
    app.config.from_object(get_config(config))
    boostrap_db(app)
    configure_cli(app)
    configure_apispec(app)
    register_blueprints(app)
    return app


def boostrap_db(app):
    """Configure flask extensions"""
    engine = create_engine(
            app.config["DATABASE_URI"],
            isolation_level="READ UNCOMMITTED",
        )
    orm.metadata.create_all(engine)
    app.config["SESSION_FACTORY"] = sessionmaker(
        bind=engine
    )
    orm.start_mappers()
    # migrate.init_app(app, db) has to be external to flask


def configure_cli(app):
    """Configure Flask 2.0's cli for easy entity management"""
    app.cli.add_command(manage.init)


def configure_apispec(app):
    """Configure APISpec for swagger support"""
    apispec.init_app(app)
#     apispec.spec.components.security_scheme(
#         "jwt", {"type": "http", "scheme": "bearer", "bearerFormat": "JWT"}
#     )
#     apispec.spec.components.schema(
#         "PaginatedResult",
#         {
#             "properties": {
#                 "total": {"type": "integer"},
#                 "pages": {"type": "integer"},
#                 "next": {"type": "string"},
#                 "prev": {"type": "string"},
#             }
#         },
#     )


def register_blueprints(app):
    """Register all blueprints for application"""
    app.register_blueprint(api.views.blueprint)
