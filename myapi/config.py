"""Default configuration

Use env var to override
"""
import os



class Config(object):
    ENV = os.getenv("FLASK_ENV")
    SQLALCHEMY_TRACK_MODIFICATIONS = True


class ProductionConfig(Config):
    DATABASE_URI = "sqlite:///db.sqlite"
    DEBUG = True
    # DB_ENGINE=create_engine(
    #         DATABASE_URI,
    #         isolation_level="READ UNCOMMITTED",
    #     )



class DevelopmentConfig(Config):
    DATABASE_URI = "sqlite:///:memory:"
    DEBUG = True



class TestingConfig(Config):
    DATABASE_URI = "sqlite:///:memory:"
    TESTING = True
    DEBUG = True



def get_config(config_name):
    configs = {
        "production": ProductionConfig,
        "development": DevelopmentConfig,
        "test": TestingConfig,
    }
    if config_name in configs:
        return configs[config_name]
    raise ValueError("Unknown config name : {}".format(config_name))
