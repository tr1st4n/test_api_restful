from marshmallow import Schema, fields
from pydantic.dataclasses import dataclass

class NodeSchema(Schema):
    serial_number = fields.Str(required=True)
    hostname = fields.Str()
    install_progress = fields.Int()
    model = fields.Str()

@dataclass
class Node:
    serial_number :str
    hostname : str = None
    install_progress : int = 0
    model : str = None