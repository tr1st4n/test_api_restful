from dataclasses import dataclass


@dataclass
class Node:
    serial_number: str
    hostname: str
    model: str
    install_progress: int
