from sqlalchemy import Table, MetaData, Column, Integer, String, Date, ForeignKey
from sqlalchemy.orm import mapper, declarative_base

from myapi.domain import model
from pprint import pprint

metadata = MetaData()

nodes = Table(
    "nodes",
    metadata,
    Column("id", Integer, primary_key=True, autoincrement=True),
    Column("serial_number", String(255), nullable=False),
    Column("hostname", String(255), nullable=False),
    Column("model", String(255), nullable=False),
    Column("install_progress" , Integer, nullable=True),
)



def start_mappers():
    mapper(model.Node, nodes)