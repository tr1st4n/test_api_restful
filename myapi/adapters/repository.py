import abc
from myapi.domain import model


class AbstractRepository(abc.ABC):
    @abc.abstractmethod
    def add(self, node: model.Node):
        raise NotImplementedError

    @abc.abstractmethod
    def get_by_serial(self, serial_number) -> model.Node:
        raise NotImplementedError

    @abc.abstractmethod
    def get_by_hostname(self, hostname) -> model.Node:
        raise NotImplementedError


class SqlAlchemyRepository(AbstractRepository):
    def __init__(self, session):
        self.session = session

    def add(self, node):
        self.session.add(node)

    def delete(self, node):
        self.session.delete(node)

    def get_by_serial(self, serial_number):
        return (
            self.session.query(model.Node)
            .filter_by(serial_number=serial_number)
            .first()
        )

    def get_by_hostname(self, hostname):
        return self.session.query(model.Node).filter_by(hostname=hostname).first()
