from flask import Blueprint, current_app, jsonify
from flask_restful import Api
from pydantic import ValidationError
from myapi.extensions import apispec
from myapi.api.resources import NodeResource
from myapi.schemas import NodeSchema, Node


blueprint = Blueprint("api", __name__, url_prefix="/api/v1")
api = Api(blueprint)


api.add_resource(NodeResource, "/node/<string:serial_number>", endpoint="node_by_serial")


@blueprint.before_app_first_request
def register_views():
    # apispec.spec.components.schema("NodeSchema", schema=NodeSchema)
    apispec.spec.components.schema("Node", schema=Node)
    apispec.spec.path(view=NodeResource, app=current_app)


@blueprint.errorhandler(ValidationError)
def handle_marshmallow_error(e):
    """Return json error for marshmallow validation errors.

    This will avoid having to try/catch ValidationErrors in all endpoints, returning
    correct JSON response with associated HTTP 400 Status (https://tools.ietf.org/html/rfc7231#section-6.5.1)
    """
    return jsonify(e.messages), 400
