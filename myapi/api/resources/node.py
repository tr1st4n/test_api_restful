from flask import request, current_app
from flask_restful import Resource
from myapi.service_layer import services, unit_of_work
from myapi.schemas import Node, NodeSchema
from myapi.exceptions import NodeNotFoundError
from http import HTTPStatus
class NodeResource(Resource):
    """Single object resource

    ---
    get:
      tags:
        - api
      summary: Get a user
      description: Get a single user by ID
      parameters:
        - in: path
          name: serial_number
          schema:
            type: string
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  user: Node
        404:
          description: user does not exists
    """

    def get(self, serial_number):
        schema = NodeSchema()
        try:
            node = services.get_node(
                unit_of_work.SqlAlchemyUnitOfWork(current_app.config["SESSION_FACTORY"]),
                # **schema.loads(serial_number))
                serial_number)
        except NodeNotFoundError as exc:
            return {"message": "node not found", "params": serial_number}, HTTPStatus.NOT_FOUND
        return {"user": Node(**node).json}, HTTPStatus.OK
    

