from __future__ import annotations
from typing import Optional
from datetime import date

from myapi.domain import model
from myapi.service_layer import unit_of_work
from myapi.exceptions import NodeNotFoundError

def create_node(
    uow: unit_of_work.AbstractUnitOfWork,
    serial_number: str,
    hostname: str,
    model: str,
):
    with uow:
        node = uow.nodes.get_by_serial(serial_number)
        if node is None:
            node = model.Node(serial_number, hostname, model)
            uow.nodes.add(node)
        uow.commit()


def update_node(
    uow: unit_of_work.AbstractUnitOfWork,
    progression: int,
    serial_number: str = "",
    hostname: str = "",
):
    with uow:
        node = uow.nodes.get_by_serial(serial_number)
        if not node:
            node = uow.get_by_hostname(hostname)
        if node is not None:
            node.install_progress = progression
            uow.nodes.add(node)
            uow.commit()
        raise NodeNotFoundError("Node not found with following inputs: serial={}/hostname={}".format(serial_number, hostname))

def delete_node(
    uow: unit_of_work.AbstractUnitOfWork,
    serial_number: str = "",
    hostname: str = "",
):
    with uow:
        node = uow.nodes.get_by_serial(serial_number)
        if not node:
            node = uow.nodes.get_by_hostname(hostname)
        if node is not None:
            uow.nodes.delete(node)
        uow.commit()


def get_node(
    uow: unit_of_work.AbstractUnitOfWork,
    serial_number: str = "",
    hostname: str = "",
):
    with uow:
        node = uow.nodes.get_by_serial(serial_number)
        if not node:
            node = uow.nodes.get_by_hostname(hostname)
        if not node:
            raise NodeNotFoundError("Node not found with following inputs: serial={}/hostname={}".format(serial_number, hostname))
        return node
